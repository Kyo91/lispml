#|
  This file is a part of lispml project.
  Copyright (c) 2018 Sam Blumenthal (sam.sam.42@gmail.com)
|#

#|
  Author: Sam Blumenthal (sam.sam.42@gmail.com)
|#

(defsystem "lispml"
  :version "0.0.1"
  :author "Sam Blumenthal"
  :license "BSD-3"
  :depends-on ("clml" "alexandria")
  :components ((:module "src"
                :components
                ((:file "clustering")
		 (:file "lispml"))))
  :description ""
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "lispml-test"))))
