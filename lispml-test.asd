#|
  This file is a part of lispml project.
  Copyright (c) 2018 Sam Blumenthal (sam.sam.42@gmail.com)
|#

(defsystem "lispml-test"
  :defsystem-depends-on ("prove-asdf")
  :author "Sam Blumenthal"
  :license "BSD-3"
  :depends-on ("lispml"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "lispml"))))
  :description "Test system for lispml"

  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
